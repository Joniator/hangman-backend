# Working with the database

The server creates a SQLite database called hangman.db with the needed tables if they don't exist.

After initial creation, you can stop the server and add your users to the `hangman_users` table, and your words to the `hangman_word` table respectively.

If the server starts, it looks for a file called words.txt in the working directory. If it finds it, it will load the words from the file in the database.
The file should contain one word per line.

<aside class="warning">
You can not edit the database while the server is running
</aside> 
