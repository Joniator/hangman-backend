# Configure the app

> Running the app on a custom port with a custom word file

```shell--script
HANGMAN_WORD_LIST="my_words.txt" HANGMAN_PORT=54321 java -jar target/hangman-0.0.10-SNAPSHOT.jar
```

The app can be configured with environment variables.

| Environment variable    | Default   | Description  
| ----------------------- | --------- | -----------
| HANGMAN_PORT            | 12345     | The port the application will listen on
| HANGMAN_MAX_GUESSES     | 6         | The amount of wrong guesses that are allowed
| HANGMAN_WORD_LIST       | words.txt | The path of the file containing the words that should be loaded
| HANGMAN_NEW_ROUND_DELAY | 5000   | Time in ms between rounds
