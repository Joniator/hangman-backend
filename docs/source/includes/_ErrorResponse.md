# Error-Response

> Example ErrorResponse if the json was malformed

```json--response
{
  "type": "ErrorResponse",
  "message": "Error parsing json" 
}
```

> (Maybe a newline in the request terminated the command early)

Every Command might return an ErrorResponse in case 
there was a syntax error in the json or there where required fields missing.

Field        | Description
------------ | -----------
type | Type of the response, in this case `ErrorResponse`
message      | A message briefly describing the nature of the request (For details check server log)
