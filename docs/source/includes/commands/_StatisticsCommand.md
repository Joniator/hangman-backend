## Statistics-Command

### Request Parameters

> Gets the wins/played games of an user:

```json
{ 
  "command": "Statistics",
  "payload": {
    "login": "<username>"
  }
}
```

```shell
echo '{"command": "Statistics", "payload": { "login": "<username>"}' | nc -q0 localhost 12345
```

| Parameter name | Description 
| -------------- | ------------
| login          | Username of the account you want to get the statistics of

### Response fields

> Sample responses

```json--response
{
  "type": "StatisticsResponse",
  "totalGames": <total-games>,
  "wonGames": <won-games>
}
```


Field        | Description
------------ | -----------
type         | Type of the response, `StatisticsResponse` if succesfull or [`ErrorResponse`](#error-response) on error
totalGames   | Total games played by the player
wonGames     | Games won by the player
