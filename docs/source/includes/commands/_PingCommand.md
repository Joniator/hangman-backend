## Ping-Command

### Request Parameters

> Ping the server

```json
{ 
  "command": "ping"
}
```

```shell
echo '{"command": "ping"}' | nc -q0 localhost 12345
```

None

### Response fields

> Sample Responses

```json--response
{
  "type": "PingResponse",
  "status": "HEALTHY",
  "login": null
}
```

```json--response
{
  "type": "PingResponse",
  "status": "HEALTHY",
  "login": "<username>"
}
``` 


Field        | Description
------------ | -----------
type         | Type of the response, `LoginResponse` if succesfull or [`ErrorResponse`](#error-response) on error
status       | Status of the server, `HEALHTY`, `ERROR` or `STARTING`
login        | If logged in, returns the username, otherwise returns null
