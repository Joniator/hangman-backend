## State-Command

### Request Parameters

> Gets the current state of the game:

```json
{ 
  "command": "State"
}
```

```shell
echo '{"command": "State"}' | nc -q0 localhost 12345
```

| Parameter name | Description 
| -------------- | ------------
| -              | -

### Response fields

> Sample responses

```json--response
{
  "type": "StateResponse,
  "players": [ "<player" ],
  "wordLength": <word-length>, 
  "wrongGuesses": <wrong-guesses>,
  "chars": [ '<char>', null, 'R' ], 
  "invalidChars": ['<char>'],
  "invalidWords": ["<word>"]
}
```


Field        | Description
------------ | -----------
type         | Type of the response, `StateResponse` if succesfull or [`ErrorResponse`](#error-response) on error
players      | An array of the players currently connected
wordLength   | Length of the word to be guessed
wrongGuesses | Amount of wrong guesses the user submitted
chars        | Array of guessed chars of the word, null if the char is not guessed yet
invalidChars | An array of wrongly guessed characters
invalidWords | An array of wrongly guessed Words  
