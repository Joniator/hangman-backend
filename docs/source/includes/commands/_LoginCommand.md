## Login-Command

### Request Parameters

> Log in as a user:

```json
{ 
  "command": "login", 
  "payload": {
    "username": "<username>", 
    "password": "<password>"
  }
}
```

```shell
echo '{"command": "login", "payload": {"username": "<username>", "password": "<password>"}}' | nc -q0 localhost 12345
```

| Parameter name | Description 
| -------------- | ------------
| username       | 
| password       |

### Response fields

> Sample responses

```json--response
{
  "type":"LoginResponse",
  "ok": true,
  "message":"Login erfolgreich" 
}
```

```json--response
{
  "type": "LoginResponse",
  "ok": false,
  "message": "Login ungültig" 
}
```

Field        | Description
------------ | -----------
type | Type of the response, `LoginResponse` if succesfull or [`ErrorResponse`](#error-response) on error
ok           | `true` if login was succesfull, `false` if login failed
message      | A message describing the result
