## CheckGuess-Command

### Request Parameters

> Notifies the server about a guess:

```json
{ 
  "command": "CheckGuess",
  "payload": {
    "guessType": "WORD | CHAR",
    "content": "<word> | <char>"
  }
}
```

```shell
echo '{"command": "CheckGuess"}' | nc -q0 localhost 12345
```

| Parameter name | Description 
| -------------- | ------------
| guessType      | `WORD` if you guess a word, `CHAR` if you guess a char
| content        | The word or char you guessed

### Response fields

> Sample responses

```json--response
{
  "type": "CheckGuessResponse",
  "correct": <true | false>,
}
```


Field        | Description
------------ | -----------
type         | Type of the response, `CheckGuessResponse` if succesfull or [`ErrorResponse`](#error-response) on error
correct      | `true` if the guess was correct, `false` if the guess was wrong
