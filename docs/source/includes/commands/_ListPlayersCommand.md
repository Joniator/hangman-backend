## ListPlayers-Command

### Request Parameters

> Get a list of current players

```json
{ 
  "command": "ListPlayers"
}
```

```shell
echo '{"command": "ListPlayers"}' | nc -q0 localhost 12345
```


| Parameter name | Description 
| -------------- | ------------
| -              | -

### Response fields

> Sample response

```json--response
{
  "type": "ListPlayersResponse",
  "players": [ "user1", "user2" ]
}
```

Field        | Description
------------ | -----------
type         | State
players      | Array of the players in the current game
