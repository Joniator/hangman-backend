## WrongGuessEvent

> Example event

```json--example
{
  "type": "WrongGuessEvent",
  "guessType": "WORD | CHAR",
  "content": "<word> | <char>"
}
```

This event is raised if a player guessed a wrong word or char.

Field        | Description
------------ | -----------
type         | Type of the Event, `WrongGuessEvent`
guessType    | `WORD` if the guess was a word, `CHAR` if it was a character
content      | The word or char that was wrongly guessed 
