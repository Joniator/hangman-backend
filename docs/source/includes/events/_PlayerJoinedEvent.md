## PlayerJoinedEvent

> Example event

```json--example
{
  "type": "PlayerJoinedEvent",
  "playerName": "<username>"
}
```

This event is raised if a player joined the game

Field        | Description
------------ | -----------
type    | Type of the Event, `PlayerJoinedEvent`
playerName   | The name of the player who just joined
