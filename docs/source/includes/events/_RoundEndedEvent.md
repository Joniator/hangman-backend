## RoundEndedEvent

> Example event

```json--example
{
  "type": "RoundEndedEvent",
  "wordGuessed": <true|false>,
  "correctWord": "<word>"
  "winner": "<username>",
}
```


This event is raised if the round ends because the word was guessed or everybody was hanged.

Field        | Description
------------ | -----------
type         | Type of the Event, `RoundEndedEvent`
wordGuessed  | `true` if the word was guessed, `false` if everyone was hanged
correctWord  | The word that was guessed
winner       | Username of the winner if the word was guessed, or `null` if everybody was hanged 
