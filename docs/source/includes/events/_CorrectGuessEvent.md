## CorrectGuessEvent

> Example event

```json--example
{
  "type": "CorrectGuessEvent",
  "guessedChar": "<char>",
  "positions": [ <positions> ]
}
```


This event is raised if a player guessed a correct char.

Field        | Description
------------ | -----------
type         | Type of the Event, `CorrectGuessEvent`
guessedChar  | The character that was guessed
positions    | The positions in the word the guessed character appears (Starts with 0) 
