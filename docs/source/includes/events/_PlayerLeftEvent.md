## PlayerLeftEvent

> Example event

```json--example
{
  "type": "PlayerLeftEvent",
  "playerName": "<username>"
}
```

This event is raised if a player left the game

Field        | Description
------------ | -----------
type         | Type of the Event, `PlayerLeftEvent`
playerName   | The name of the player who just left
