## ShutdownEvent

> Example event

```json--example
{
  "type": "ShutdownEvent",
  "message": "Server is shutting down"
}
```

This event is raised if the server shuts down

Field        | Description
------------ | -----------
type         | Type of the Event, `ShutdownEvent`
message      | A message.
