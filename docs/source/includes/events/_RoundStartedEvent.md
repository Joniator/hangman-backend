## RoundStartedEvent

> Example event

```json--example
{
  "type": "RoundStartedEvent",
  "wordLength": <length>
}
```

This event is raised if the round starts, either if the first player connects or after the last rounds winner is announced.

Field        | Description
------------ | -----------
type         | Type of the Event, `RoundStartedEvent`
wordLength   | The length of the new word
