---
title: API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - json
  - shell

toc_footers:
  - <a href='https://github.com/slatedocs/slate'>Documentation Powered by Slate</a>

includes:
  - Config
  - Database

  - headers/Commands
  - commands/PingCommand
  - commands/LoginCommand
  - commands/ListPlayersCommand
  - commands/CheckGuessCommand
  - commands/StateCommand
  - commands/StatisticsCommand
  
  - headers/Events
  - events/ShutdownEvent
  - events/RoundStartedEvent
  - events/RoundEndedEvent
  - events/PlayerJoinedEvent
  - events/PlayerLeftEvent
  - events/CorrectGuessEvent
  - events/WrongGuessEvent
  
  - ErrorResponse.md

search: true
---

# Introduction

## Requirements 

The app requires *Java 11 or newer*.  

## Running the app

The pre-built jar can be downloaded [from the release page.](https://gitlab.com/Joniator/hangman-backend/-/releases)  
To start the jar, run `java -jar target/hangman-*.jar`

## Build it yourself

Clone or download the repository. 

To start the app, run `./mvwn spring-boot:run`  
The configuration file can be found at `src/main/resources/application.yml`

To build the jar, run `./mvnw package`
To start the built jar, run `java -jar target/hangman-*.jar`

## The API

> Command structure

```json
{
  "command": "<command-name>",
  "payload": {}
}
```

```shell
echo '{"command": "<command-name>", "payload": "<payload-object>}' | nc -q0 localhost 12345
```

The app listens on port 12345 to any TCP request.
It expects the connected client to send commands in json format, and responds to them. 
The server will also send a json payload on events that happen in the game, like another player guessing a letter.

<aside class="info">
The server expects the json to be in a single line (not containing newlines or linebreaks) a request to ends with the first newline.
</aside>

## Implemented

### Commands

- [x] PingCommand
- [x] LoginCommand
- [x] ListPlayersCommand
- [x] StateCommand
- [x] StatisticCommand
- [x] CheckGuessCommand

### Events

- [x] RoundStartEvent
- [x] RoundEndEvent
- [x] CorrectGuessEvent
- [x] WrongGuessEvent
- [x] PlayerJoinedEvent
- [x] PlayerLeftEvent
