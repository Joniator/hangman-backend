package bws.fiae.hangman;

public enum GuessType {
    WORD,
    CHAR
}
