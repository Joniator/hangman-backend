package bws.fiae.hangman;

import bws.fiae.hangman.config.HangmanConfig;
import bws.fiae.hangman.config.SQLiteLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;

@SpringBootApplication
public class HangmanApplication implements CommandLineRunner {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	final ServerListener serverListener;
	static Connection connection;

	private static HangmanConfig config;

	public HangmanApplication(ServerListener serverListener, HangmanConfig config) {
		this.serverListener = serverListener;
		this.config = config;
	}

	public static HangmanConfig getConfig() {
		return config;
	}

	public static Connection getConnection() {
		return connection;
	}

	@Override
	public void run(String... args) {
		try {
			connection = DriverManager.getConnection(config.getConnectionString());
			SQLiteLoader sqLiteLoader = new SQLiteLoader(connection);
			sqLiteLoader.prepare();
		} catch (SQLException throwables) {
			log.error("Can't load SQLite connection");
		}

		Thread serverThread = new Thread(serverListener);
		serverThread.setName("ListenerThread");
		serverThread.start();
	}

	public static void main(String[] args) {
		SpringApplication.run(HangmanApplication.class, args);
	}
}
