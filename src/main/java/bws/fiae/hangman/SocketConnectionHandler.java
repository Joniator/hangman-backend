package bws.fiae.hangman;

import bws.fiae.hangman.commands.*;
import bws.fiae.hangman.events.BaseEvent;
import bws.fiae.hangman.core.GameService;
import bws.fiae.hangman.core.SessionContext;
import bws.fiae.hangman.responses.ErrorResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class SocketConnectionHandler implements Runnable {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Socket socket;

    private BufferedReader reader;
    private BufferedWriter writer;

    private final SessionContext context;
    private boolean isConnected = false;

    public SocketConnectionHandler(Socket socket) {
        this.socket = socket;
        context = new SessionContext();
    }

    public void close() {
        log.debug("Connection closed");
        try {
            socket.close();
        } catch (IOException e) {
            log.debug("Connection could not be closed", e);
        }
    }

    @Override
    public void run() {
        String hostAddress = socket.getInetAddress().getHostAddress();
        Thread.currentThread().setName("ct " + hostAddress);
        log.info("Handling connection for client {}", hostAddress);
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            isConnected = true;
            context.setWriter(writer);
            String line;
            while ((line = reader.readLine()) != null) {
                log.debug("Message received: {}", line);
                handleJson(line);
            }
        } catch (SocketTimeoutException e) {
            log.info("Client timed out");
        } catch (IOException e) {
            log.error("Error processing socket streams", e);
        }
        isConnected = false;
        GameService.getInstance().removeConnection(this);
        GameService.getInstance().removePlayer(context.getLoggedInUser());
        log.info("Client disconnected");
    }

    private void handleJson(String json) {
        JsonNode jsonNode;
        try {
            jsonNode = objectMapper.readValue(json, JsonNode.class);
        } catch (JsonProcessingException e) {
            context.sendResponse(new ErrorResponse("Error parsing json"));
            log.debug("Error parsing json in {}", json);
            return;
        }

        if (jsonNode.get("command") == null) {
            context.sendResponse(new ErrorResponse("No command field found"));
            log.debug("Error parsing json No command field found in {}", json);
            return;
        }

        switch (jsonNode.get("command").asText("").toLowerCase()) {
            case "ping":
                new PingCommand(context).handleCommand(jsonNode);
                break;
            case "login":
                new LoginCommand(context).handleCommand(jsonNode);
                break;
            case "listplayers":
                new ListPlayersCommand(context).handleCommand(jsonNode);
                break;
            case "checkguess":
                new CheckGuessCommand(context).handleCommand(jsonNode);
                break;
            case "state":
                new StateCommand(context).handleCommand(jsonNode);
                break;
            case "statistics":
                new StatisticsCommand(context).handleCommand(jsonNode);
                break;
            default:
                ErrorResponse errorResponse = new ErrorResponse("Command not supported");
                context.sendResponse(errorResponse);
                break;
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    public SessionContext getContext() {
        return context;
    }

    public <T extends BaseEvent> void sendEvent(T event) {
        context.sendEvent(event);
    }
}
