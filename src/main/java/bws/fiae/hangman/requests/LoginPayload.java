package bws.fiae.hangman.requests;

public class LoginPayload {
    String username;
    String password;

    public LoginPayload() {
        // Empty constructor for jackson
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
