package bws.fiae.hangman.requests;

import bws.fiae.hangman.GuessType;

public class CheckGuessPayload {
    GuessType guessType;
    String content;

    public CheckGuessPayload() {
    }

    public GuessType getGuessType() {
        return guessType;
    }

    public String getContent() {
        return content.toLowerCase();
    }
}
