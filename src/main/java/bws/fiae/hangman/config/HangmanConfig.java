package bws.fiae.hangman.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.yml")
public class HangmanConfig {
    @Value("${hangman.sql.connection-string}")
    private String connectionString;

    @Value("${hangman.max-guesses:6}")
    private int maxGuesses;

    @Value("${hangman.word-list:words.txt}")
    private String wordList;

    @Value("${hangman.new-round-delay:5000}")
    private long newRoundDelay;

    public String getConnectionString() {
        return connectionString;
    }

    public int getMaxGuesses() {
        return maxGuesses;
    }

    public String getWordListPath() {
        return wordList;
    }

    public long getNewRoundDelay() {
        return newRoundDelay;
    }
}
