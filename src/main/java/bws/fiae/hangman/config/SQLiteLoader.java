package bws.fiae.hangman.config;

import bws.fiae.hangman.HangmanApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteLoader {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final HangmanConfig config;
    private final Connection connection;

    public SQLiteLoader(Connection connection) {
        this.connection = connection;
        config = HangmanApplication.getConfig();
    }


    public void prepare() {
            try {
                Statement statement = connection.createStatement();
                log.info("Preparing database {}", config.getConnectionString());
                createUserTable(statement);
                createWordTable(statement);
                fillWordTable(statement);
                log.info("Prepared database {}", config.getConnectionString());
            } catch (SQLException e) {
                log.error("Could not set up SQLite db", e);
            }
    }

    private void fillWordTable(Statement statement) throws SQLException {
        try {
            FileInputStream is = new FileInputStream(config.getWordListPath());
            InputStreamReader reader = new InputStreamReader(is);
            BufferedReader buf = new BufferedReader(reader);
            while (buf.ready()) {
                String word = buf.readLine();
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO hangman_words (word) SELECT ?" +
                        " WHERE NOT EXISTS(SELECT 1 FROM hangman_words WHERE word=?)");
                preparedStatement.setString(1, word);
                preparedStatement.setString(2, word);
                preparedStatement.executeUpdate();
            }
        } catch (FileNotFoundException e) {
            log.error("Could not load word file, server starting with existing words in db");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createWordTable(Statement statement) throws SQLException {
        log.info("Creating hangman_words table");
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS hangman_words" +
                "(word VARCHAR(255))");
    }

    private void createUserTable(Statement statement) throws SQLException {
        log.info("Creating hangman_users table");
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS hangman_users" +
                "(username VARCHAR(255) not null " +
                "constraint hangman_users_pk primary key," +
                "password VARCHAR(255) not null," +
                "games_won int," +
                "games_played int);");
    }
}
