package bws.fiae.hangman;

import bws.fiae.hangman.core.GameService;

public class ShutdownRunner implements Runnable {

    @Override
    public void run() {
        GameService.getInstance().shutdown();
    }
}
