package bws.fiae.hangman.events;

import bws.fiae.hangman.events.PlayerJoinedEvent;

public class PlayerLeftEvent extends PlayerJoinedEvent {
    public PlayerLeftEvent() {
        super();
    }

    public PlayerLeftEvent(String username) {
        super(username);
    }
}
