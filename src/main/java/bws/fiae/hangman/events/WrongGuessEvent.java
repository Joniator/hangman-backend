package bws.fiae.hangman.events;

import bws.fiae.hangman.GuessType;

public class WrongGuessEvent extends BaseEvent {
    GuessType guessType;
    String content;

    public WrongGuessEvent() {
        super();
    }

    public WrongGuessEvent(GuessType guessType, String content) {
        this.guessType = guessType;
        this.content = content;
    }

    public GuessType getGuessType() {
        return guessType;
    }

    public String getContent() {
        return content;
    }
}
