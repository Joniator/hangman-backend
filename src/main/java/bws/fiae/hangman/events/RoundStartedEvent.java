package bws.fiae.hangman.events;

public class RoundStartedEvent extends BaseEvent {
    int wordLength;

    public RoundStartedEvent(String word) {
        super();
        wordLength = word.length();
    }

    public int getWordLength() {
        return wordLength;
    }
}
