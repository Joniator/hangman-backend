package bws.fiae.hangman.events;

public class RoundEndedEvent extends BaseEvent {
    boolean wordGuessed;
    String correctWord;
    String winner;

    public RoundEndedEvent() {
        super();
    }

    public RoundEndedEvent(boolean wordGuessed, String correctWord, String winner) {
        super();
        this.wordGuessed = wordGuessed;
        this.correctWord = correctWord;
        this.winner = winner;
    }

    public boolean isWordGuessed() {
        return wordGuessed;
    }

    public String getCorrectWord() {
        return correctWord;
    }

    public String getWinner() {
        return winner;
    }
}
