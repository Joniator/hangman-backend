package bws.fiae.hangman.events;

import java.util.Objects;

public class PlayerJoinedEvent extends BaseEvent {
    public String playerName;

    public PlayerJoinedEvent() {
        super();
    }

    public PlayerJoinedEvent(String playerName) {
        super();
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerJoinedEvent that = (PlayerJoinedEvent) o;
        return Objects.equals(playerName, that.playerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerName);
    }
}
