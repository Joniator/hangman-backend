package bws.fiae.hangman.events;

import java.util.List;

public class CorrectGuessEvent extends BaseEvent {
    char guessedChar;
    List<Integer> positions;

    public CorrectGuessEvent() {
        super();
    }

    public CorrectGuessEvent(char guessedChar, List<Integer> positions) {
        this.guessedChar = guessedChar;
        this.positions = positions;
    }

    public char getGuessedChar() {
        return guessedChar;
    }

    public List<Integer> getPositions() {
        return positions;
    }
}
