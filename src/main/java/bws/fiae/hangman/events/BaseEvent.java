package bws.fiae.hangman.events;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseEvent {
    @JsonProperty("type")
    public String eventType;

    public BaseEvent() {
        eventType = this.getClass().getSimpleName();
    }

    public String getEventType() {
        return eventType;
    }
}
