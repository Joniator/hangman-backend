package bws.fiae.hangman.events;

public class ShutdownEvent extends BaseEvent {
    String message = "Server is shutting down";
    public ShutdownEvent() {
        super();
    }

    public String getMessage() {
        return message;
    }
}
