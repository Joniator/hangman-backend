package bws.fiae.hangman.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponse {
    @JsonProperty("type")
    String responseType;

    public BaseResponse() {
        responseType = this.getClass().getSimpleName();
    }

    public String getResponseType() {
        return responseType;
    }
}
