package bws.fiae.hangman.responses;

import java.util.Objects;

public class ErrorResponse extends BaseResponse {
    String message;

    public ErrorResponse() {
        super();
        // Empty constructor for jackson
    }

    public ErrorResponse(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorResponse that = (ErrorResponse) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }
}
