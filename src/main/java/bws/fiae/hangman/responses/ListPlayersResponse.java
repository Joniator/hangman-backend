package bws.fiae.hangman.responses;

import java.util.List;
import java.util.Objects;

public class ListPlayersResponse extends BaseResponse {
    List<String> players;

    public ListPlayersResponse() {
    }

    public ListPlayersResponse(List<String> players) {
        this.players = players;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListPlayersResponse that = (ListPlayersResponse) o;
        return Objects.equals(players, that.players);
    }

    @Override
    public int hashCode() {
        return Objects.hash(players);
    }
}
