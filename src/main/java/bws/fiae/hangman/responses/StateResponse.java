package bws.fiae.hangman.responses;

import java.util.List;

public class StateResponse extends BaseResponse {
    List<String> players;
    int wordLength;
    int wrongGuesses;
    List<Character> chars;
    List<Character> invalidChars;
    List<String> invalidWords;

    public StateResponse() {
        super();
    }

    public StateResponse(List<String> players, int wordLength, int wrongGuesses, List<Character> chars, List<Character> invalidChars, List<String> invalidWords) {
        this.players = players;
        this.wordLength = wordLength;
        this.wrongGuesses = wrongGuesses;
        this.chars = chars;
        this.invalidChars = invalidChars;
        this.invalidWords = invalidWords;
    }

    public List<String> getPlayers() {
        return players;
    }

    public int getWordLength() {
        return wordLength;
    }

    public int getWrongGuesses() {
        return wrongGuesses;
    }

    public List<Character> getChars() {
        return chars;
    }

    public List<Character> getInvalidChars() {
        return invalidChars;
    }

    public List<String> getInvalidWords() {
        return invalidWords;
    }
}
