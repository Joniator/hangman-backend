package bws.fiae.hangman.responses;

import java.util.Objects;

public class LoginResponse extends BaseResponse {
    boolean ok;
    String message;

    public LoginResponse() {
        super();
    }

    public LoginResponse(Boolean ok, String message) {
        this.ok = ok;
        this.message = message;
    }

    public boolean isOk() {
        return ok;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginResponse that = (LoginResponse) o;
        return ok == that.ok &&
              Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ok, message);
    }
}
