package bws.fiae.hangman.responses;

import java.util.Objects;

public class PingResponse extends BaseResponse {
    PingResponseStatus status;
    String login;

    public PingResponse() {
        super();
    }

    public PingResponse(PingResponseStatus status, String login) {
        super();
        this.status = status;
        this.login = login;
    }

    public PingResponseStatus getStatus() {
        return status;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PingResponse that = (PingResponse) o;
        return status == that.status &&
                Objects.equals(login, that.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, login);
    }
}
