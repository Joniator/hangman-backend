package bws.fiae.hangman.responses;

public class CheckGuessResponse extends BaseResponse {
    boolean correct;
    public CheckGuessResponse() {
        super();
    }

    public CheckGuessResponse(boolean correct) {
        this.correct = correct;
    }

    public boolean isCorrect() {
        return correct;
    }
}
