package bws.fiae.hangman.responses;

public class StatisticsResponse extends BaseResponse {
    int totalGames;
    int wonGames;

    public StatisticsResponse() {
        super();
    }

    public StatisticsResponse(int totalGames, int wonGames) {
        this.totalGames = totalGames;
        this.wonGames = wonGames;
    }

    public int getTotalGames() {
        return totalGames;
    }

    public int getWonGames() {
        return wonGames;
    }
}
