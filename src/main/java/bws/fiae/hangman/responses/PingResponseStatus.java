package bws.fiae.hangman.responses;

public enum PingResponseStatus {
    HEALTHY,
    STARTING,
    ERROR
}
