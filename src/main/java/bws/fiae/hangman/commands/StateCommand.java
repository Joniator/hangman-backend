package bws.fiae.hangman.commands;

import bws.fiae.hangman.core.GameService;
import bws.fiae.hangman.core.SessionContext;
import bws.fiae.hangman.responses.BaseResponse;
import bws.fiae.hangman.responses.StateResponse;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class StateCommand extends BaseCommand {
    final GameService gameService;
    public StateCommand(SessionContext context) {
        super(context);
        gameService = GameService.getInstance();
    }

    @Override
    public void handleCommand(JsonNode json) {
        List<String> players = gameService.getPlayers();
        int wordLength = gameService.getWordLength();
        int wrongGuesses = context.getWrongGuesses();
        List<Character> chars = gameService.getGuessedChars(),
                invalidChars = gameService.getWrongChars();
        List<String> invalidWords = gameService.getWrongWords();
        BaseResponse response = new StateResponse(players,
                wordLength,
                wrongGuesses,
                chars,
                invalidChars,
                invalidWords);

        context.sendResponse(response);
    }
}
