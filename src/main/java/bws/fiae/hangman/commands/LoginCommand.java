package bws.fiae.hangman.commands;

import bws.fiae.hangman.core.GameService;
import bws.fiae.hangman.core.SessionContext;
import bws.fiae.hangman.core.User;
import bws.fiae.hangman.core.UserService;
import bws.fiae.hangman.requests.LoginPayload;
import bws.fiae.hangman.responses.ErrorResponse;
import bws.fiae.hangman.responses.LoginResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

public class LoginCommand extends BaseCommand {
    UserService userService;
    GameService gameService;


    public LoginCommand(SessionContext context) {
        super(context);
        userService = UserService.getInstance();
        gameService = GameService.getInstance();
    }

    @Override
    public void handleCommand(JsonNode json) {
        try {
            JsonNode payload = json.get("payload");
            if (payload == null) {
                context.sendResponse(new ErrorResponse("Payload missing"));
            }
            LoginPayload loginPayload = objectMapper.treeToValue(payload, LoginPayload.class);
            if (loginPayload.getUsername() == null || loginPayload.getPassword() == null) {
                context.sendResponse(new ErrorResponse("Username or password can not be null"));
            }
            else if (loginPayload.getUsername().length() >= 30) {
                context.sendResponse(new LoginResponse(false, "Username muss unter 30 Zeichen lang sein :)"));
            }
            else if (userService.checkLogin(loginPayload.getUsername(), loginPayload.getPassword())) {
                User user = userService.getUser(loginPayload.getUsername());
                context.sendResponse(new LoginResponse(true, "Login erfolgreich"));
                context.setLoggedInUser(user);
                gameService.addPlayer(user);
            }
            else {
                context.sendResponse(new LoginResponse(false, "Login ungültig"));
            }
        } catch (JsonProcessingException e) {
            log.error("Can't parse login payload");
            context.sendResponse(new ErrorResponse("Can't parse login payload"));
        }
    }
}
