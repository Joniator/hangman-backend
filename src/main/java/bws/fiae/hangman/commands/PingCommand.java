package bws.fiae.hangman.commands;

import bws.fiae.hangman.core.SessionContext;
import bws.fiae.hangman.responses.PingResponse;
import bws.fiae.hangman.responses.PingResponseStatus;
import com.fasterxml.jackson.databind.JsonNode;

public class PingCommand extends BaseCommand {

    public PingCommand(SessionContext context) {
        super(context);
    }

    @Override
    public void handleCommand(JsonNode json) {
        PingResponse response = new PingResponse(PingResponseStatus.HEALTHY, context.isLoggedIn() ? context.getLoggedInUser().getUsername() : null);
        context.sendResponse(response);
    }
}
