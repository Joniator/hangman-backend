package bws.fiae.hangman.commands;

import bws.fiae.hangman.core.GameService;
import bws.fiae.hangman.core.SessionContext;
import bws.fiae.hangman.core.User;
import bws.fiae.hangman.core.UserService;
import bws.fiae.hangman.requests.StatisticsPayload;
import bws.fiae.hangman.responses.BaseResponse;
import bws.fiae.hangman.responses.ErrorResponse;
import bws.fiae.hangman.responses.StateResponse;
import bws.fiae.hangman.responses.StatisticsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class StatisticsCommand extends BaseCommand {
    final GameService gameService;

    public StatisticsCommand(SessionContext context) {
        super(context);
        gameService = GameService.getInstance();
    }

    @Override
    public void handleCommand(JsonNode json) {
        try {
            JsonNode payload = json.get("payload");
            if (payload == null) {
                context.sendResponse(new ErrorResponse("Payload missing"));
            }
            StatisticsPayload statisticsPayload =
                  objectMapper.treeToValue(payload, StatisticsPayload.class);
            User user;
            if (statisticsPayload.getLogin() == null | statisticsPayload.getLogin() == "") {
                  user = context.getLoggedInUser();
            } else {
                user = UserService.getInstance().getUser(statisticsPayload.getLogin());
            }
            if (user == null) {
                context.sendResponse(new ErrorResponse("User doesn't exist"));
            }

            int gamesPlayed = user.getGamesPlayed();
            int gamesWon = user.getGamesWon();
            BaseResponse response = new StatisticsResponse(gamesPlayed, gamesWon);

            context.sendResponse(response);
        } catch (JsonProcessingException e) {
            log.error("Can't parse statistics payload");
            context.sendResponse(new ErrorResponse("Can't parse login payload"));
        }
    }
}
