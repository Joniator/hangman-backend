package bws.fiae.hangman.commands;

import bws.fiae.hangman.GuessType;
import bws.fiae.hangman.core.GameService;
import bws.fiae.hangman.core.SessionContext;
import bws.fiae.hangman.core.UserService;
import bws.fiae.hangman.events.CorrectGuessEvent;
import bws.fiae.hangman.events.WrongGuessEvent;
import bws.fiae.hangman.requests.CheckGuessPayload;
import bws.fiae.hangman.responses.CheckGuessResponse;
import bws.fiae.hangman.responses.ErrorResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class CheckGuessCommand extends BaseCommand {
    final GameService gameService;
    final UserService userService;

    private static final ReentrantLock mutex = new ReentrantLock();

    public CheckGuessCommand(SessionContext context) {
        super(context);
        gameService = GameService.getInstance();
        userService = UserService.getInstance();
    }

    @Override
    public synchronized void handleCommand(JsonNode json) {
        try {
            mutex.lock();
            boolean isGuessed = false;
            JsonNode payload = json.get("payload");
            if (payload == null) {
                context.sendResponse(new ErrorResponse("Payload missing"));
                return;
            }
            CheckGuessPayload checkGuessPayload = objectMapper.treeToValue(payload, CheckGuessPayload.class);
            if (checkGuessPayload.getGuessType() == null ||
                  checkGuessPayload.getContent() == null) {
                context.sendResponse(new ErrorResponse("Incomplete payload"));
                return;
            }

            if (context.noGuessLeft()) {
                context.sendResponse(new ErrorResponse("You don't have any guesses left"));
                return;
            }

            if (!checkGuessPayload.getContent().matches("[a-zA-Z]+")) {
                context.sendResponse(new ErrorResponse("Content contains illegal characters"));
                return;
            }

            /**
             * Check character guess
             */
            if (checkGuessPayload.getGuessType() == GuessType.CHAR) {
                if (checkGuessPayload.getContent().length() != 1) {
                    log.error("Content is not a char");
                    context.sendResponse(new ErrorResponse("Content is not a char"));
                    return;
                }
                char character = checkGuessPayload.getContent().charAt(0);
                List<Integer> positions = gameService.checkChar(character);
                if (!positions.isEmpty()) {
                    /**
                     * Correct guess
                     */
                    context.sendResponse(new CheckGuessResponse(true));
                    gameService.sendPublicEvent(new CorrectGuessEvent(character, positions));
                } else {
                    /**
                     * Wrong guess
                     */
                    context.sendResponse(new CheckGuessResponse(false));
                    context.setWrongGuesses(context.getWrongGuesses() + 1);
                    gameService.sendPublicEvent(
                          new WrongGuessEvent(GuessType.CHAR,
                                Character.toString(character)));
                }
                if (gameService.wordIsGuessed()) {
                    isGuessed = true;
                }
            }

            /**
             * Check word guess
             */
            if (checkGuessPayload.getGuessType() == GuessType.WORD) {
                if (checkGuessPayload.getContent().length() < 1) {
                    log.error("Content is not a word");
                    context.sendResponse(new ErrorResponse("Content is not a word"));
                    return;
                }

                String word = checkGuessPayload.getContent().toLowerCase();
                if (gameService.checkWord(word)) {
                    /**
                     * Correct guess
                     */
                    context.sendResponse(new CheckGuessResponse(true));
                    isGuessed = true;
                } else {
                    /**
                     * Wrong guess
                     */
                    context.sendResponse(new CheckGuessResponse(false));
                    context.setWrongGuesses(context.getWrongGuesses() + 1);
                    gameService.sendPublicEvent(new WrongGuessEvent(GuessType.WORD, word));
                }
            }

            if (isGuessed) {
                gameService.endRound(true, gameService.getWord(), context.getLoggedInUser().getUsername());
            } else if (gameService.roundIsLost()) {
                gameService.endRound(false, gameService.getWord(), null);
            }
        } catch (JsonProcessingException e) {
            log.error("Can't parse checkguess payload");
            context.sendResponse(new ErrorResponse("Can't parse checkguess payload"));
        }
        finally {
            mutex.unlock();
        }
    }
}
