package bws.fiae.hangman.commands;

import bws.fiae.hangman.core.GameService;
import bws.fiae.hangman.core.SessionContext;
import bws.fiae.hangman.responses.ListPlayersResponse;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class ListPlayersCommand extends BaseCommand {
    final GameService gameService;

    public ListPlayersCommand(SessionContext context) {
        super(context);
        gameService = GameService.getInstance();
    }

    @Override
    public void handleCommand(JsonNode json) {
        List<String> users = gameService.getPlayers();
        context.sendResponse(new ListPlayersResponse(users));
    }
}
