package bws.fiae.hangman.commands;

import bws.fiae.hangman.core.SessionContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseCommand {
    Logger log = LoggerFactory.getLogger(this.getClass());
    ObjectMapper objectMapper = new ObjectMapper();

    SessionContext context;

    public BaseCommand(SessionContext context) {
        this.context = context;
    }

    public abstract void handleCommand(JsonNode json);
}
