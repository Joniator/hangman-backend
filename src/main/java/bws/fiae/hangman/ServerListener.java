package bws.fiae.hangman;

import bws.fiae.hangman.core.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ServerListener implements Runnable {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${hangman.port}")
    private int port;
    private ServerSocket serverSocket;
    private ExecutorService executorService;

    @Override
    public void run()  {
        // Create TCP server socket and listen on configured port
        if (serverSocket == null) {
            try {
                serverSocket = new ServerSocket(port);
                log.info("Hangman server listening on {}", port);
            } catch (IOException e) {
                log.error("Port {} already in use", port, e);
                return;
            }
        }

        // Create thread pool for incoming connections
        if (executorService == null) {
            log.debug("CachedThreadPool created");
            executorService = Executors.newCachedThreadPool();
        }

        GameService gameService = GameService.getInstance();

        Runtime.getRuntime().addShutdownHook(new Thread(new ShutdownRunner()));

        while (!serverSocket.isClosed()) {
            try {
                // Accept a client and launch a new thread processing the client
                Socket socket = serverSocket.accept();
                SocketConnectionHandler socketConnectionHandler = new SocketConnectionHandler(socket);
                executorService.execute(socketConnectionHandler);
                gameService.registerConnection(socketConnectionHandler);
            } catch (IOException e) {
                log.error("Error accepting and handling client connection", e);
            }
        }
    }
}
