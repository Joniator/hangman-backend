package bws.fiae.hangman.core;

import bws.fiae.hangman.HangmanApplication;

public class StartRoundDelayed implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(HangmanApplication.getConfig().getNewRoundDelay());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameService.getInstance().startRound();
    }
}
