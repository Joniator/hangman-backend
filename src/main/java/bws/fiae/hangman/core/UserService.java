package bws.fiae.hangman.core;

import bws.fiae.hangman.HangmanApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class UserService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static UserService instance;
    private Connection connection;

    private UserService() {
        connection = HangmanApplication.getConnection();
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public User getUser(String username) {
        User user = null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM hangman_users WHERE username=?1");
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.username = resultSet.getString("username");
                user.password = resultSet.getString("password");
                user.gamesPlayed = resultSet.getInt("games_played");
                user.gamesWon = resultSet.getInt("games_won");
            }
        } catch (SQLException e) {
            log.error("Error executing sql", e);
        }
        return user;
    }

    public boolean checkLogin(String username, String password) {
        User user = getUser(username);
        return user != null &&
                user.getPassword().equals(password);
    }

    public void addGameLost(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE hangman_users " +
                    "SET games_played=games_played+1 " +
                    "WHERE username=?1");
            statement.setString(1, user.getUsername());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error executing sql", e);
        }
    }

    public void addGameWon(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE hangman_users " +
                    "SET games_played=games_played+1," +
                    "games_won=games_won+1 " +
                    "WHERE username=?1");
            statement.setString(1, user.getUsername());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error executing sql", e);
        }
    }
}
