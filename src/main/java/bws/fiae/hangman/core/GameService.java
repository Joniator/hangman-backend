package bws.fiae.hangman.core;

import bws.fiae.hangman.HangmanApplication;
import bws.fiae.hangman.events.*;
import bws.fiae.hangman.SocketConnectionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GameService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    final UserService userService;
    static GameService instance;
    Connection connection;

    List<SocketConnectionHandler> connections;
    List<User> players = new ArrayList<>();

    List<String> wordList = new ArrayList<>();
    String word;
    boolean roundIsRunning = false;
    List<String> wrongWords = new ArrayList<>();
    List<Character> wrongChars = new ArrayList<>();
    List<Character> guessedChars = new ArrayList<>();


    public static GameService getInstance() {
        if (instance == null)
            instance = new GameService();
        return instance;
    }

    private GameService() {
        userService = UserService.getInstance();
        connection = HangmanApplication.getConnection();
        connections = new ArrayList<>();
        try {
            FileReader reader = new FileReader("words.txt");
        } catch (FileNotFoundException e) {
            log.error("Wort-Datei kann nicht geöffnet werden.");
        }

    }

    public List<String> getPlayers() {
        return players.stream()
              .map(p -> p.username)
              .collect(Collectors.toList());
    }

    public int getWordLength() {
        if (word != null)
            return word.length();
        return 0;
    }

    public List<Character> getGuessedChars() {
        List<Character> response = new ArrayList<>();
        for (Character character : word.toCharArray()) {
            if (guessedChars.contains(character)) {
                response.add(character);
            }
            else {
                response.add(null);
            }
        }
        return response;
    }

    public List<Character> getWrongChars() {
        return wrongChars;
    }

    public List<String> getWrongWords() {
        return wrongWords;
    }

    public String getWord() {
        return word;
    }

    public void addPlayer(User user) {
        List<User> matched = players.stream()
              .filter(u -> u.getUsername().equals(user.getUsername()))
              .collect(Collectors.toList());
        if (!matched.isEmpty()) {
            removePlayer(matched.get(0));
        }
        players.add(user);
        sendPublicEvent(new PlayerJoinedEvent(user.getUsername()));

        if (players.size() == 1) {
            startRound();
        }
    }

    public void removePlayer(User loggedInUser) {
        SocketConnectionHandler handler = null;
        if (loggedInUser == null) {
            return;
        }
        for (SocketConnectionHandler connection : connections) {
            User connectionUser = connection.getContext().getLoggedInUser();
            if (connectionUser != null &&
                  connectionUser.equals(loggedInUser)) {
                connection.close();
                handler = connection;
            }
        }
        if (roundIsRunning) {
            userService.addGameLost(loggedInUser);
        }
        players.remove(loggedInUser);
        if (handler != null) {
            connections.remove(handler);
        }
        sendPublicEvent(new PlayerLeftEvent(loggedInUser.getUsername()));
        if (roundIsLost()) {
            endRound(false, word, null);
        }
    }

    public boolean roundIsLost() {
        // Check if the other players can guess
        boolean someoneCanGuess = false;
        for (SocketConnectionHandler con : connections) {
            if (!con.getContext().noGuessLeft()) {
                someoneCanGuess = true;
                break;
            }
        }
        return !someoneCanGuess;
    }

    public void registerConnection(SocketConnectionHandler socketConnectionHandler) {
        connections.add(socketConnectionHandler);
    }

    public void removeConnection(SocketConnectionHandler socketConnectionHandler) {
        connections.remove(socketConnectionHandler);
    }

    public void startRound() {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT word FROM hangman_words ORDER BY RANDOM() LIMIT 1");

            if (resultSet.next()) {
                word = resultSet.getString("word").toLowerCase();
            } else {
                log.error("Could not start round, no words in database!");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        wrongChars.clear();
        wrongWords.clear();
        guessedChars.clear();
        connections.forEach(con -> con.getContext().setWrongGuesses(0));
        roundIsRunning = true;

        sendPublicEvent(new RoundStartedEvent(word));
        log.info("Started round, the word to guess is {}", word);
    }

    public void endRound(boolean wordGuessed, String correctWord, String winner) {
        roundIsRunning = false;
        players.forEach(user -> {
            if (user.getUsername().equals(winner)) {
                userService.addGameWon(user);
            } else {
                userService.addGameLost(user);
            }
        });
        sendPublicEvent(new RoundEndedEvent(wordGuessed, correctWord, winner));

        Thread thread = new Thread(new StartRoundDelayed());
        thread.start();
    }

    public List<Integer> checkChar(Character character) {
        List<Integer> resultList = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == Character.toLowerCase(character)) {
                resultList.add(i);
                if (!guessedChars.contains(character))
                    guessedChars.add(character);
            }
        }
        if (resultList.size() == 0) {
            wrongChars.add(character);
        }
        return resultList;
    }

    public boolean wordIsGuessed() {
        if (!roundIsRunning) {
            return false;
        }
        for (Character character : word.toCharArray()) {
            if (!guessedChars.contains(character)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkWord(String word) {
        if (roundIsRunning && word.toLowerCase().equals(this.word.toLowerCase())) {
            return true;
        }
        wrongWords.add(word);
        return false;
    }

    public void shutdown() {
        sendPublicEvent(new ShutdownEvent());
    }

    public <T extends BaseEvent> void sendPublicEvent(T event) {
        sendPublicEvent(event, new ArrayList<>());
    }

    public <T extends BaseEvent> void sendPublicEvent(T event, List<User> except) {
        List<SocketConnectionHandler> receivers = connections.stream()
              .filter(c -> !except.contains(c.getContext().getLoggedInUser()))
              .collect(Collectors.toList());
        for (SocketConnectionHandler connection : receivers) {
            connection.sendEvent(event);
        }
    }
}
