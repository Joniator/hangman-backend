package bws.fiae.hangman.core;

import bws.fiae.hangman.HangmanApplication;
import bws.fiae.hangman.events.BaseEvent;
import bws.fiae.hangman.responses.BaseResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.UUID;

public class SessionContext {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    ObjectMapper objectMapper;
    UUID sessionId;
    User user;
    int wrongGuesses;
    BufferedWriter writer;

    public SessionContext() {
        sessionId = UUID.randomUUID();
        objectMapper = new ObjectMapper();
        wrongGuesses = 0;
    }

    public boolean noGuessLeft() {
        return wrongGuesses >= HangmanApplication.getConfig().getMaxGuesses();
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public User getLoggedInUser() {
        return user;
    }

    public void setLoggedInUser(User user) {
        this.user = user;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public int getWrongGuesses() {
        return wrongGuesses;
    }

    public void setWrongGuesses(int wrongGuesses) {
        this.wrongGuesses = wrongGuesses;
    }

    public BufferedWriter getWriter() {
        return writer;
    }

    public void setWriter(BufferedWriter writer) {
        this.writer = writer;
    }

    public <T extends BaseResponse> void sendResponse(T response) {
        try {
            String responseString = objectMapper.writeValueAsString(response);
            writer.write(responseString);
            writer.newLine();
            writer.flush();
            log.debug("Send {}: {}", response.getClass().getSimpleName(), responseString);
        } catch (JsonProcessingException e) {
            log.error("Could not serialize response", e);
        } catch (IOException e) {
            log.error("Failed to write response to output stream", e);
        }
    }

    public <T extends BaseEvent> void sendEvent(T event) {
        if (writer == null) {
            try {
                Thread.sleep(200);
                if (writer == null) {
                    log.debug("Could not send Event because writer was not ready");
                    return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            String responseString = objectMapper.writeValueAsString(event);
            writer.write(responseString);
            writer.newLine();
            writer.flush();
            log.debug("Send {}: {}", event.getClass().getSimpleName(), responseString);
        } catch (JsonProcessingException e) {
            log.error("Could not serialize response", e);
        } catch (IOException e) {
            log.error("Failed to write response to output stream", e);
        }
    }

}
