package bws.fiae.hangman.config;

import bws.fiae.hangman.responses.BaseResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;

public class TestSocket {
    private final String hostname;
    private final int port;

    Socket socket;
    BufferedReader reader;
    BufferedWriter writer;

    TestSocket(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }

    public void connect() throws IOException {
        if (socket != null && socket.isConnected()) {
            socket.close();
        }
        socket = new Socket(hostname, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public void close() throws IOException, InterruptedException {
        socket.close();
        Thread.sleep(100);
    }

    public String sendCommand(String json) throws IOException {
        writer.write(json);
        writer.newLine();
        writer.flush();
        return reader.readLine();
    }

    public String readLine() throws IOException {
        return reader.readLine();
    }

    public <T extends BaseResponse> T sendCommand(String json, Class<T> expectedResponse) throws IOException {
        String response = sendCommand(json);
        ObjectMapper oj = new ObjectMapper();
        return oj.readValue(response, expectedResponse);
    }
}
