package bws.fiae.hangman.events;

import bws.fiae.hangman.HangmanApplicationTests;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

public class PlayerJoinedEventTest extends HangmanApplicationTests {
    @Test
    @Disabled
    void testLogin() throws IOException {
        ArrayList<String> responses = new ArrayList<>();
        responses.add(socket.sendCommand("{\"command\": \"login\", \"payload\": { \"username\": \"user\", \"password\": \"user\" }}"));
        responses.add(socket.readLine());
    }
}
