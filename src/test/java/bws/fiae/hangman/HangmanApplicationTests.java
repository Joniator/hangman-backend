package bws.fiae.hangman;

import bws.fiae.hangman.config.TestConfig;
import bws.fiae.hangman.config.TestSocket;
import bws.fiae.hangman.responses.ErrorResponse;
import bws.fiae.hangman.responses.LoginResponse;
import bws.fiae.hangman.responses.PingResponse;
import bws.fiae.hangman.responses.PingResponseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@Import(TestConfig.class)
@Timeout(value = 5)
public class HangmanApplicationTests {
	@Autowired
	protected TestSocket socket;

	protected ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	protected void init() throws IOException {
		socket.connect();
	}

	@AfterEach
	protected void teardown() throws IOException, InterruptedException {
		socket.close();
	}
}
