package bws.fiae.hangman.commands;

import bws.fiae.hangman.HangmanApplicationTests;
import bws.fiae.hangman.config.TestConfig;
import bws.fiae.hangman.config.TestSocket;
import bws.fiae.hangman.responses.LoginResponse;
import bws.fiae.hangman.responses.PingResponse;
import bws.fiae.hangman.responses.PingResponseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Import(TestConfig.class)
@Timeout(value = 5)
class PingCommandTest extends HangmanApplicationTests {
    @Test
    void testBeforeLogin() throws IOException {
        PingResponse expectedResponse = new PingResponse(PingResponseStatus.HEALTHY, null);

        String rawResponse = socket.sendCommand("{\"command\": \"ping\"}");
        PingResponse actualResponse = objectMapper.readValue(rawResponse, PingResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    @Disabled("Rework test")
    void testAfterSuccessfullLogin() throws IOException {
        PingResponse expectedResponse = new PingResponse(PingResponseStatus.HEALTHY, "user");

        LoginResponse loginResponse = socket.sendCommand("{\"command\": \"login\", \"payload\": { \"username\": \"user\", \"password\": \"user\" }}", LoginResponse.class);
        assertThat(loginResponse.isOk()).isTrue();

        PingResponse actualResponse = socket.sendCommand("{\"command\": \"ping\"}", PingResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testAfterUnsuccessfullLogin() throws IOException {
        PingResponse expectedResponse = new PingResponse(PingResponseStatus.HEALTHY, null);

        LoginResponse loginResponse = socket.sendCommand("{\"command\": \"login\", \"payload\": { \"username\": \"user\", \"password\": \"wrongPassword\" }}", LoginResponse.class);
        assertThat(loginResponse.isOk()).isFalse();

        PingResponse actualResponse = socket.sendCommand("{\"command\": \"ping\"}", PingResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }
}
