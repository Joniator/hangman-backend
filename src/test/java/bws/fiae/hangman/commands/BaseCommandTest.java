package bws.fiae.hangman.commands;

import bws.fiae.hangman.HangmanApplicationTests;
import bws.fiae.hangman.responses.ErrorResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class BaseCommandTest extends HangmanApplicationTests {
    @Test
    void testNoJson() throws IOException {
        String requestJson = "{ This is not valid json }";
        ErrorResponse expectedResponse = new ErrorResponse("Error parsing json");

        String rawResponse = socket.sendCommand(requestJson);
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testNoCommand() throws IOException {
        String requestJson = "{ \"message\": \"This json is lacking a command field\"}";
        ErrorResponse expectedResponse = new ErrorResponse("No command field found");

        String rawResponse = socket.sendCommand(requestJson);
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testNotExistingCommand() throws IOException {
        String requestJson = "{ \"command\": \"This command does not exist\"}";
        ErrorResponse expectedResponse = new ErrorResponse("Command not supported");

        String rawResponse = socket.sendCommand(requestJson);
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }
}