package bws.fiae.hangman.commands;

import bws.fiae.hangman.HangmanApplicationTests;
import bws.fiae.hangman.config.TestConfig;
import bws.fiae.hangman.config.TestSocket;
import bws.fiae.hangman.responses.ErrorResponse;
import bws.fiae.hangman.responses.LoginResponse;
import bws.fiae.hangman.responses.PingResponse;
import bws.fiae.hangman.responses.PingResponseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Import(TestConfig.class)
@Timeout(value = 5)
class LoginCommandTest extends HangmanApplicationTests {
    public static String login(TestSocket socket, String username, String password) throws IOException {
        return socket.sendCommand(String.format("{\"command\": \"login\", \"payload\": { \"username\": \"%s\", \"password\": \"%s\" }}", username, password));
    }

    public String login(String username, String password) throws IOException {
        return login(socket, username, password);
    }

    @Test
    void testMissingPayload() throws IOException {
        ErrorResponse expectedResponse = new ErrorResponse("Payload missing");

        String rawResponse = socket.sendCommand("{\"command\": \"login\"}");
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testUsernameMissing() throws IOException {
        ErrorResponse expectedResponse = new ErrorResponse("Username or password can not be null");

        String rawResponse = socket.sendCommand("{\"command\": \"login\", \"payload\": {\"password\": \"pw\" }}");
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        boolean yes = actualResponse.equals(expectedResponse);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testUsernameNull() throws IOException {
        ErrorResponse expectedResponse = new ErrorResponse("Username or password can not be null");

        String rawResponse = socket.sendCommand("{\"command\": \"login\", \"payload\": { \"username\": null, \"password\": \"pw\" }}");
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testPasswordMissing() throws IOException {
        ErrorResponse expectedResponse = new ErrorResponse("Username or password can not be null");

        String rawResponse = socket.sendCommand("{\"command\": \"login\", \"payload\": { \"username\": \"user\"}}");
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testPasswordNull() throws IOException {
        ErrorResponse expectedResponse = new ErrorResponse("Username or password can not be null");

        String rawResponse = socket.sendCommand("{\"command\": \"login\", \"payload\": { \"username\": \"user\", \"password\": null }}");
        ErrorResponse actualResponse = objectMapper.readValue(rawResponse, ErrorResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testWrongPassword() throws IOException {
        LoginResponse expectedResponse = new LoginResponse(false, "Login ungültig");

        String rawResponse = login("user", "falsch");
        LoginResponse actualResponse = objectMapper.readValue(rawResponse, LoginResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    @Disabled
    void testSuccess() throws IOException {
        LoginResponse expectedResponse = new LoginResponse(true, "Login erfolgreich");

        String event = login("user", "user");
        String rawResponse = socket.readLine();
        LoginResponse actualResponse = objectMapper.readValue(rawResponse, LoginResponse.class);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }
}
