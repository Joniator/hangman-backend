package bws.fiae.hangman.commands;

import bws.fiae.hangman.HangmanApplicationTests;
import bws.fiae.hangman.config.TestConfig;
import bws.fiae.hangman.config.TestSocket;
import bws.fiae.hangman.responses.ListPlayersResponse;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Import(TestConfig.class)
@Timeout(value = 5)
class ListPlayersCommandTest extends HangmanApplicationTests {
    @Autowired
    TestSocket loginSocket;

    @Test
    void testNoPlayers() throws IOException {
        ListPlayersResponse expectedResponse = new ListPlayersResponse(new ArrayList<>());

        String rawResponse = socket.sendCommand("{\"command\": \"ListPlayers\"}");
        ListPlayersResponse actualResponse = objectMapper.readValue(rawResponse, ListPlayersResponse.class);
        assertThat(actualResponse.getPlayers()).asList().isEqualTo(expectedResponse.getPlayers());
    }

    @Test
    @Disabled
    void testOnePlayer() throws IOException {
        String username = "user";
        ArrayList<String> players = new ArrayList();
        players.add(username);
        ListPlayersResponse expectedResponse = new ListPlayersResponse(players);

        LoginCommandTest.login(socket, username, username);
        socket.readLine();
        String rawResponse = socket.sendCommand("{\"command\": \"ListPlayers\"}");
        ListPlayersResponse actualResponse = objectMapper.readValue(rawResponse, ListPlayersResponse.class);
        assertThat(actualResponse.getPlayers()).asList().isEqualTo(expectedResponse.getPlayers());
    }

    @Test
    @Disabled
    void testOnePlayerLoggedOut() throws IOException, InterruptedException {
        String username = "user";
        ArrayList<String> players = new ArrayList<>();
        ListPlayersResponse expectedResponse = new ListPlayersResponse(players);
        loginSocket.connect();

        LoginCommandTest.login(loginSocket, username, username);
        socket.readLine();
        loginSocket.close();
        socket.readLine();
        String rawResponse = socket.sendCommand("{\"command\": \"ListPlayers\"}");
        ListPlayersResponse actualResponse = objectMapper.readValue(rawResponse, ListPlayersResponse.class);
        assertThat(actualResponse.getPlayers()).asList().isEqualTo(expectedResponse.getPlayers());
    }

    @Test
    @Disabled
    void testTwoPlayer() throws IOException, InterruptedException {
        String user1 = "user1";
        String user2 = "user2";
        ArrayList<String> players = new ArrayList<>();
        players.add(user1);
        players.add(user2);
        ListPlayersResponse expectedResponse = new ListPlayersResponse(players);
        loginSocket.connect();

        LoginCommandTest.login(loginSocket, user1, user1);
        socket.readLine();
        LoginCommandTest.login(socket, user2, user2);
        socket.readLine();
        String rawResponse = socket.sendCommand("{\"command\": \"ListPlayers\"}");
        loginSocket.close();
        ListPlayersResponse actualResponse = objectMapper.readValue(rawResponse, ListPlayersResponse.class);
        assertThat(actualResponse.getPlayers()).asList().isEqualTo(expectedResponse.getPlayers());
    }

    @Test
    @Disabled
    void testTwoPlayerLoggedOut() throws IOException, InterruptedException {
        String logoutUser = "logout";
        String user = "user";
        ArrayList<String> players = new ArrayList<>();
        players.add(user);
        ListPlayersResponse expectedResponse = new ListPlayersResponse(players);
        loginSocket.connect();

        LoginCommandTest.login(loginSocket, logoutUser, logoutUser);
        loginSocket.close();
        LoginCommandTest.login(socket, user, user);
        String rawResponse = socket.sendCommand("{\"command\": \"ListPlayers\"}");
        ListPlayersResponse actualResponse = objectMapper.readValue(rawResponse, ListPlayersResponse.class);
        assertThat(actualResponse.getPlayers()).asList().isEqualTo(expectedResponse.getPlayers());
    }
}
